# WooCommerce Order Manager #

The plugin lists all upcoming orders for today or by date. Each order contains a custom meta information where each customer provided a preferred delivery date. The plugin also lists each intersection products from all orders in a given day.

It requires WooCommerce to be installed before. 