<?php

/*
Plugin Name: WooCommerce Upcoming Orders
Description: List upcoming orders for today or search them by available dates
Version: 1.0
Author: Botez Costin
*/

// only if WooCommerce is active

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	/**
	 * Load plugin's textdomain
	 *
	 * @return void
	 */
	function upcoming_orders_load_textdomain() {
		load_plugin_textdomain( 'woocommerce-upcoming-orders', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
	add_action( 'plugins_loaded', 'upcoming_orders_load_textdomain' );

	/**
	 * ENQUEUE SCRIPTS IN ADMIN
	 *
	 * @return void
	 */
	function fitfoodie_admin_style($hook) {
		if(in_array($hook, array('dashboard_page_upcoming-orders', 'dashboard_page_grouped-products'))) {
			wp_enqueue_style( 'bootstrap-css', plugin_dir_url( __FILE__ ) . '/css/bootstrap.min.css' );
			wp_enqueue_style( 'chosen-css', plugin_dir_url( __FILE__ ) . '/js/chosen/chosen.min.css' );
			wp_enqueue_style( 'style-css', plugin_dir_url( __FILE__ ) . '/css/style.css' );

		    wp_register_script( 'bootstrap-js', plugin_dir_url( __FILE__ ) . '/js/bootstrap.min.js');
		    wp_enqueue_script( 'bootstrap-js' );

		    wp_register_script( 'admin-js', plugin_dir_url( __FILE__ ) . '/js/admin-scripts.js');
		    wp_enqueue_script( 'admin-js' );

		    wp_register_script( 'chosen-js', plugin_dir_url( __FILE__ ) . '/js/chosen/chosen.jquery.min.js');
		    wp_enqueue_script( 'chosen-js' );
		}
	}
	add_action( 'admin_enqueue_scripts', 'fitfoodie_admin_style' );

	/**
	 * Add order delivery date field
	 *
	 * @version 1.0
	 * @return HTML
	 */
	function fitfoodie_checkout_date_field($checkout) { ?>
		<script>
			jQuery(function() {
				jQuery( "#datepicker" ).datepicker({
					minDate: +1,
			    	maxDate: '+1M',
			    	beforeShowDay: jQuery.datepicker.noWeekends
				});
			});
		</script>
		<div id="fitfoodie_delivery_date">
		<?php
			woocommerce_form_field( 'fitfoodie_delivery_date', array(
				'type' 			=> 'text',
				'class' 		=> array('fitfoodie_delivery_date form-row-wide'),
				'id'            => 'datepicker',
				'label' 		=> __('Pick a delivery date:', 'fitfoodie'),
				'placeholder'   => __('Select Date', 'fitfoodie'),
				'required'  	=> true,
				),
				$checkout->get_value( 'fitfoodie_delivery_date' )
			);
		?>
		</div>
		<?php
	}
	add_action('woocommerce_after_order_notes', 'fitfoodie_checkout_date_field');

	/**
	 * Add validation on delivery date when process checkout.
	 *
	 * @version 1.0
	 **/
	function fitfoodie_checkout_delivery_date_process() {

		// Check if set, if its not set add an error.
		if (!$_POST['fitfoodie_delivery_date'])
			wc_add_notice( __( 'Please choose a delivery date.', 'fitfoodie' ), 'error' );
	}
	add_action('woocommerce_checkout_process', 'fitfoodie_checkout_delivery_date_process');

	/**
	 * Update the order meta with field value
	 **/
	function fitfoodie_checkout_delivery_date_update_order_meta( $order_id ) {
		if ($_POST['fitfoodie_delivery_date'])
			update_post_meta( $order_id, 'fitfoodie_delivery_date', esc_attr($_POST['fitfoodie_delivery_date']));
	}
	add_action('woocommerce_checkout_update_order_meta', 'fitfoodie_checkout_delivery_date_update_order_meta');

	require_once('modules/upcoming-orders.php');
	require_once('modules/grouped-products.php');

}
?>