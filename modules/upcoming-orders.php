<?php

// ----------------- UPCOMING PRODUCTS --------------
//
/**
 * Admin menu page for upcoming orders
 *
 * @version 1.0
 * @return WP_Post (Page)
 */
function fitfoodie_admin_menu_order_reports() {
	add_submenu_page(
		'index.php',
		__('Upcoming orders', 'fitfoodie'),
		__('Upcoming orders', 'fitfoodie'),
		'manage_options',
		'upcoming-orders',
		'fitfoodie_admin_menu_order_reports_cb'
	);
}
add_action('admin_menu', 'fitfoodie_admin_menu_order_reports');


/**
 * Callback for upcoming orders
 *
 * @author Botez Costin
 * @version 1.0
 * @return HTML
 */
function fitfoodie_admin_menu_order_reports_cb() {

	// $today = getdate();
	$args = array(
				'post_type'		=> 	'shop_order',
				'post_status' 	=> 	array('processing'),
				'posts_per_page'=> 	-1,
				'meta_key'		=> 'fitfoodie_delivery_date',
				'meta_value'	=> date('m/d/Y')
			);
	$query = new WP_Query($args);
	$orders = $query->posts;

	$args = array(
				'post_type'		=> 	'shop_order',
				'post_status' 	=> 	array('processing'),
				'posts_per_page'=> 	-1
			);
	$query = new WP_Query($args);
	$orders_to_search = $query->posts;

	// echo '<pre>'; print_r(count($orders)); echo '</pre>'; exit;
	?>

	<?php $active_first_tab 	= ($_SERVER['HTTP_REFERER'] == admin_url('index.php?page=upcoming-orders') ? '' : 'active'); ?>
	<?php $active_second_tab	= ($_SERVER['HTTP_REFERER'] == admin_url('index.php?page=upcoming-orders') ? 'active' : ''); ?>

	<div>
		<h3><?php _e('Upcoming Orders', 'fitfoodie'); ?></h3>
		<ul class="nav nav-tabs">
	    	<li class="<?php echo $active_first_tab; ?>"><a href="#tabs-1" data-toggle="tab"><?php _e('Today\'s orders', 'fitfoodie'); ?></a></li>
	    	<li class="<?php echo $active_second_tab; ?>"><a href="#tabs-2" data-toggle="tab"><?php _e('View by date', 'fitfoodie'); ?></a></li>
	  	</ul>
	  	<div class="tab-content">
		  	<div class="tab-pane <?php echo $active_first_tab; ?>" id="tabs-1">
		  		<?php fitfoodie_table_show_orders_by_list($orders); ?>
		  		<button class="btn btn-primary" onclick="window.print()">
					<?php _e('Print today\'s orders'); ?>
				</button>
		  	</div>
		  	<div class="tab-pane <?php echo $active_second_tab; ?>" id="tabs-2">
				<?php if(!empty($orders_to_search)) : ?>
		  		<div class="col-lg-12">
		  			<h4><?php _e('Orders filtered by date', 'fitfoodie'); ?></h4>
			  		<div class="input-group">
			  			<?php // Get orders delivery date from meta ?>
			  			<?php foreach ($orders_to_search as $order) : ?>
			  				<?php if (!empty(get_post_meta($order->ID, 'fitfoodie_delivery_date', true))) : ?>
  								<?php $orders_date[] = get_post_meta($order->ID, 'fitfoodie_delivery_date', true); ?>
  							<?php endif; ?>
  						<?php endforeach; ?>
						<?php //echo '<pre>'; print_r($orders_date); echo '</pre>'; exit; ?>

  						<?php // Remove duplicates ?>
 						<?php $orders_date = array_unique($orders_date); ?>

 						<form method="POST" action="<?php echo admin_url('index.php?page=upcoming-orders'); ?>" style="margin-top: 30px">
	      					<select type="text" name="order-date" class="form-control chosen-select" data-placeholder="Choose a date...">
	      						<option value="0"><?php _e('Choose date', 'fitfoodie'); ?></option>
	      						<?php foreach ($orders_date as $order_date) : ?>
      								<?php if($_POST && isset($_POST['order-date']) && $order_date == $_POST['order-date']) : ?>
      									<option value="<?php echo $order_date; ?>" selected><?php echo $order_date; ?></option>
      								<?php else : ?>
      									<option value="<?php echo $order_date; ?>"><?php echo $order_date; ?></option>
      								<?php endif; ?>
	      						<?php endforeach; ?>
	      					</select>
		      				<span class="input-group-btn" style="display: inline-block">
		       					<input type="submit" class="btn btn-default" value="<?php echo _e('Show!', 'fitfoodie');?>" />
		      				</span>
		      			</form>
		      			<?php if($_POST && isset($_POST['order-date']) && $_POST['order-date'] != 0) : ?>
		      				<?php fitfoodie_table_show_orders_by_list($orders_to_search, $_POST['order-date']); ?>
		      			<?php endif; ?>
						<?php  //echo '<pre>'; print_r($_POST); echo '</pre>'; exit; ?>
	    			</div>
	    		</div>
				<?php else : ?>
				<p class="col-md-12" style="margin-top: 30px;"><?php _e('No orders found.', 'fitfoodie'); ?></p>
			<?php endif; ?>
	  		</div>
	  	</div>
	</div>
	<?
}

/**
 * Get customers shipping address
 *
 * @hooked filter fitfoodie_get_customer_address
 *
 * @author Botez Costin
 * @version 1.0
 * @param  Int $user_id
 * @return HTML
 */
function fitfoodie_get_customer_address($user_id) {

    $address = '';

    $address .= (!empty(get_user_meta( $user_id, 'billing_first_name', true )) ? get_user_meta( $user_id, 'billing_first_name', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_last_name', true )) ? get_user_meta( $user_id, 'billing_last_name', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_company', true )) ? get_user_meta( $user_id, 'billing_company', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_address_1', true )) ? get_user_meta( $user_id, 'billing_address_1', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_address_2', true )) ? get_user_meta( $user_id, 'billing_address_2', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_city', true )) ? get_user_meta( $user_id, 'billing_city', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_state', true )) ? get_user_meta( $user_id, 'billing_state', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_postcode', true )) ? get_user_meta( $user_id, 'billing_postcode', true ) . '<br>': '');
    $address .= (!empty(get_user_meta( $user_id, 'billing_country', true )) ? get_user_meta( $user_id, 'billing_country', true ) . '<br>': '');

    return apply_filters('fitfoodie_get_customer_address', $address);
}

/**
 * Get customers order items
 *
 *
 * @author Botez Costin
 * @version 1.5
 * @param  Object(WC_Order) $order
 * @return HTML
 */
function fitfoodie_get_customers_items($order) {
	$shown_order_id 		= false;
	$shown_order_username 	= false;
	$shown_order_address 	= false;
	$shown_order_email	 	= false;

	foreach( $order->get_items() as $item_id => $item ) {
		$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item ); ?>
		<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
			<?php if(!$shown_order_id) : ?>
				<td rowspan="<?php echo count($order->get_items()); ?>">
					<a href="<?php echo admin_url( 'post.php?post=' . absint( $order->id ) . '&action=edit' ); ?>" target="_blank"><?php echo "#$order->id"; ?></a>
				</td>
				<?php $shown_order_id = true; ?>
			<?php endif; ?>
			<?php if(!$shown_order_username) : ?>
				<td rowspan="<?php echo count($order->get_items()); ?>"><?php echo get_userdata($order->customer_user)->user_login; ?></td>
				<?php $shown_order_username = true; ?>
			<?php endif; ?>
			<?php if(!$shown_order_address) : ?>
				<td rowspan="<?php echo count($order->get_items()); ?>"><?php echo fitfoodie_get_customer_address($order->customer_user); ?></td>
				<?php $shown_order_address = true; ?>
			<?php endif; ?>
			<?php if(!$shown_order_email) : ?>
				<td rowspan="<?php echo count($order->get_items()); ?>"><?php echo get_userdata($order->customer_user)->user_email; ?></td>
				<?php $shown_order_email = true; ?>
			<?php endif; ?>
			<td class="product-name">
				<?php
					$is_visible        = $product && $product->is_visible();
					$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );

					echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item['name'] ) : $item['name'], $item, $is_visible );
					echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );

					do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

					$order->display_item_meta( $item );
					$order->display_item_downloads( $item );

					do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
				?>
			</td>
			<td class="product-total">
				<?php echo $order->get_formatted_line_subtotal( $item ); ?>
			</td>
		</tr>
		<?php
	}
	do_action( 'woocommerce_order_items_table', $order );
}

/**
 * Table of orders
 *
 * @hooked filter fitfoodie_filter_by_date
 *
 * @author Botez Costin
 * @param  Array(Object) $order
 * @param  DATE/False $order_date (Default: False)
 *
 * @return HTML
 */
function fitfoodie_table_show_orders_by_list($orders, $order_date = FALSE) {

	if(!empty($orders) && $order_date) 	{
		$args = array(
			'post_type'		=> 	'shop_order',
			'post_status' 	=> 	array('processing'),
			'posts_per_page'=> 	-1,
			'meta_key'		=> 'fitfoodie_delivery_date',
			'meta_value'	=> $order_date
		);
		$query = new WP_Query($args);
		$orders = $query->posts;

	}

	if(!empty($orders)) : ?>
  		<table class="table table-hover">
  			<thead>
  				<th><?php _e('#', 'fitfoodie'); ?></th>
  				<th><?php _e('Customer username', 'fitfoodie'); ?></th>
  				<th><?php _e('Address', 'fitfoodie'); ?></th>
  				<th><?php _e('Email', 'fitfoodie'); ?></th>
  				<th><?php _e('Order items ', 'fitfoodie'); ?></th>
  				<th><?php _e('Order totals ', 'fitfoodie'); ?></th>
  			</thead>
  			<tbody>
  				<?php foreach ($orders as $order) : ?>
  					<?php $_order = new WC_Order($order->ID); ?>
  					<?php echo fitfoodie_get_customers_items($_order); ?>
  				<?php endforeach; ?>
  			</tbody>
  		</table>
  		<?php if($order_date) : ?>
  			<button class="btn btn-primary" onclick="window.print()">
				<?php _e("Print orders from $order_date"); ?>
			</button>
		<?php endif; ?>
	<?php else : ?>
		<p class="col-md-12" style="margin-top: 30px;"><?php _e('No orders found.', 'fitfoodie'); ?></p>
	<?php endif; ?>
	<?php
}

?>