<?php

// ----------------- GROUPED PRODUCTS --------------

/**
 * Admin menu page for grouped products
 *
 * @version 1.0
 * @return WP_Post (Page)
 */
function fitfoodie_admin_menu_grouped_products_reports() {
	add_submenu_page(
		'index.php',
		__('Grouped products', 'fitfoodie'),
		__('Grouped products', 'fitfoodie'),
		'manage_options',
		'grouped-products',
		'fitfoodie_admin_menu_grouped_products_reports_cb'
	);
}
add_action('admin_menu', 'fitfoodie_admin_menu_grouped_products_reports');

/**
 * Callback for grouped products
 *
 * @author Botez Costin
 * @version 1.0
 * @return HTML
 */
function fitfoodie_admin_menu_grouped_products_reports_cb() {

	$args = array(
				'post_type'		=> 	'shop_order',
				'post_status' 	=> 	array('processing'),
				'posts_per_page'=> 	-1,
				'meta_key'		=> 'fitfoodie_delivery_date',
				'meta_value'	=> date('m/d/Y'),
			);
	$query = new WP_Query($args);
	$orders_today = $query->posts;

	$args = array(
				'post_type'		=> 	'shop_order',
				'post_status' 	=> 	array('processing'),
				'posts_per_page'=> 	-1,
				'meta_key'		=> 'fitfoodie_delivery_date',
				'meta_value'	=> date('m/d/Y', time()+86400),
			);
	$query = new WP_Query($args);
	$orders_tomorrow = $query->posts;

	$args = array(
				'post_type'		=> 	'shop_order',
				'post_status' 	=> 	array('processing'),
				'posts_per_page'=> 	-1,
				'meta_key'		=> 'fitfoodie_delivery_date',
				'orderby' 		=> 'meta_value',
        		'order' 		=> 'ASC'
			);
	$query = new WP_Query($args);
	$orders_view_by_date = $query->posts;

	// echo '<pre>'; print_r(count($orders)); echo '</pre>'; exit;
	?>

	<div>
		<h3><?php _e('Grouped products', 'fitfoodie'); ?></h3>
  		<ul class="nav nav-tabs">
	    	<li class="active"><a href="#tabs-1" data-toggle="tab"><?php _e('Today\'s products', 'fitfoodie'); ?></a></li>
	    	<li><a href="#tabs-2" data-toggle="tab"><?php _e('Tommorow\'s products', 'fitfoodie'); ?></a></li>
	    	<li><a href="#tabs-3" data-toggle="tab"><?php _e('View by date', 'fitfoodie'); ?></a></li>
	  	</ul>
	  	<div class="tab-content">
		  	<div class="tab-pane active" id="tabs-1">
  				<?php if(!empty($orders_today)) : ?>
		  			<?php fitfoodie_grouped_products_table($orders_today); ?>
		  			<button class="btn btn-primary" onclick="window.print()">
						<?php _e('Print today\'s products'); ?>
					</button>
		  		<?php else : ?>
					<p class="col-md-12" style="margin-top: 30px;"><?php _e('No products found.', 'fitfoodie'); ?></p>
				<?php endif; ?>

		  	</div>
		  	<div class="tab-pane" id="tabs-2">
		  		<?php if(!empty($orders_tomorrow)) : ?>
		  			<?php fitfoodie_grouped_products_table($orders_tomorrow); ?>
		  			<button class="btn btn-primary" onclick="window.print()">
						<?php _e('Print tommorow\'s products'); ?>
					</button>
		  		<?php else : ?>
					<p class="col-md-12" style="margin-top: 30px;"><?php _e('No products found.', 'fitfoodie'); ?></p>
				<?php endif; ?>
		  	</div>
		  	<div class="tab-pane" id="tabs-3">
		  		<?php if(!empty($orders_view_by_date)) : ?>
		  			<?php fitfoodie_grouped_products_table($orders_view_by_date, true); ?>
		  			<button class="btn btn-primary" onclick="window.print()">
						<?php _e('Print products'); ?>
					</button>
		  		<?php else : ?>
					<p class="col-md-12" style="margin-top: 30px;"><?php _e('No products found.', 'fitfoodie'); ?></p>
				<?php endif; ?>
		  	</div>
		</div>
	</div>
	<?
}

/**
 * List of grouped products
 *
 * @author Botez Costin
 * @param  Array  $orders
 * @param  Boolean $date
 * @return HTML
 */
function fitfoodie_grouped_products_table($orders, $date=FALSE) {
	if($date) {
		$orders_by_date = [];
		foreach ($orders as $order) {
			$date = get_post_meta($order->ID, 'fitfoodie_delivery_date', true);
			$orders_by_date[$date][] = $order;
		}
		//echo '<pre>'; print_r($orders_by_date); echo '</pre>'; exit;
	}
	?>
	<table class="table table-hover">
		<thead>
			<?php if($date) : ?>
			<th><?php _e('Date', 'fitfoodie'); ?></th>
			<?php endif; ?>
			<th><?php _e('Order items ', 'fitfoodie'); ?></th>
		</thead>
		<tbody>
			<?php if($date) : ?>
				<?php echo fitfoodie_get_customers_grouped_products_by_date($orders_by_date); ?>
			<?php else: ?>
				<?php foreach ($orders as $order) : ?>
					<?php $_order = new WC_Order($order->ID); ?>
					<?php echo fitfoodie_get_customers_grouped_products($_order); ?></td>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
  		</table>
  	<?php
}

/**
 * Get customers order grouped products items
 *
 * @author Botez Costin
 * @version 1.5
 * @param Object(WC_Order) $order
 * @return HTML
 */
function fitfoodie_get_customers_grouped_products($order) {
	foreach( $order->get_items() as $item_id => $item ) {
		$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item ); ?>
		<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
			<td class="product-name">
				<?php
					$is_visible        = $product && $product->is_visible();
					$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );

					echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item['name'] ) : $item['name'], $item, $is_visible );
					echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );

					do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

					$order->display_item_meta( $item );
					$order->display_item_downloads( $item );

					do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
				?>
			</td>
		</tr>
		<?php
	}
	do_action( 'woocommerce_order_items_table', $order );
}

/**
 * Get customers order grouped products items
 *
 * @author Botez Costin
 * @version 1.5
 * @param Array $order
 * @return HTML
 */
function fitfoodie_get_customers_grouped_products_by_date($order_by_date) {
	// echo '<pre>'; print_r($order_by_date); echo '</pre>';
	foreach ($order_by_date as $key => $orders) { ?>
		<tr>
			<td><?php echo $key;  ?></td>
			<td class="product-name"> <?php
			foreach ($orders as $order) {
				$_order = new WC_Order($order->ID);
				foreach( $_order->get_items() as $item_id => $item ) {
					$product = apply_filters( 'woocommerce_order_item_product', $_order->get_product_from_item( $item ), $item );
					$is_visible        = $product && $product->is_visible();
					$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $_order );

					echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item['name'] ) : $item['name'], $item, $is_visible );
					echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );

					do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $_order );

					$_order->display_item_meta( $item );
					$_order->display_item_downloads( $item );

					do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $_order );
					echo '<br>';
				}
			} ?>
			</td>
		</tr>
		<?php do_action( 'woocommerce_order_items_table', $order );
	}
}