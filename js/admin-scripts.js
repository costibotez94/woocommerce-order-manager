;(function($){
    $(document).ready(function(){
        $(".chosen-select").chosen();

	    $("tbody td").hover(function() {
			$el = $(this);
			if ($el.parent().has('td[rowspan]').length == 0)
				$el.parent().prevAll('tr:has(td[rowspan]):first').addClass("hover");

		}, function() {
			$el.parent().prevAll('tr:has(td[rowspan]):first').removeClass("hover");
		});
    });
})(jQuery);